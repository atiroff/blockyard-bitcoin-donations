<?php
/**
 * Plugin Name: Bitcoin Donations by The Blockyard
 * Plugin URI: https://www.theblockyard.io
 * Description: Add a Bitcoin donations button to a site.
 * Version: 2019.05.13.1249
 * Author: The Blockyard
 * Author URI: https://www.theblockyard.io
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       blockyard-bitcoin-donations
 * Domain Path:       /languages
 */


// This line ensures the script only runs if the WP environment is active (or something else defining ABSPATH)
defined( 'ABSPATH' ) || die;



// An inline function.  One of the only inline functions we'll use.
function Blockyard_Bitcoin_Donations_loader() {

	// These defines are common for larger plugins.  It will help us create fully qualified resource paths when needed.
	// __FILE__ is a PHP special keyword for "the current executing PHP script full qualified file name".
	defined( 'BLOCKYARD_BTC_DONATE_FILE' ) || define( 'BLOCKYARD_BTC_DONATE_FILE', __FILE__ );
	defined( 'BLOCKYARD_BTC_DONATE__DIR' ) || define( 'BLOCKYARD_BTC_DONATE__DIR' , dirname( __FILE__ ) );
}



// This tells WordPress to run our loader AFTER it has done other basic WordPress stuff.
// We want to wait until after other setup is done so we have some clue that a basic WordPress environment is intact
// before running some code that may depend on it.
//
// after_setup_theme is early enough to still take over much of WordPress but late enough to ensure at least WordPress core is running.
add_action( 'after_setup_theme' , 'Blockyard_Bitcoin_Donations_loader' );
